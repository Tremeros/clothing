import { initializeApp } from 'firebase/app';
import {
    getAuth,
    signInWithRedirect,
    signInWithPopup,
    GoogleAuthProvider
} from 'firebase/auth';
import {
    getFirestore,
    doc,
    getDoc,
    setDoc,
} from 'firebase/firestore';

const firebaseConfig = {
    apiKey: 'AIzaSyBFXEwqMBIQe5aLAU5VcYehg8i_3gfM1N0',
    authDomain: 'clothing-f580c.firebaseapp.com',
    projectId: 'clothing-f580c',
    storageBucket: 'clothing-f580c.appspot.com',
    messagingSenderId: '882988458267',
    appId: '1:882988458267:web:ed29f1d46967e47f32f3be'
};

const app = initializeApp(firebaseConfig);
console.log(app);
const provider = new GoogleAuthProvider();
provider.setCustomParameters({
    prompt: 'select_account'
});

export const auth = getAuth();

export const signInWithGooglePopup = () => signInWithPopup(auth, provider);
export const signinWithGoogleRedirect = () => signInWithRedirect(auth, provider);

export const db = getFirestore();

export const createUserDocumentFromAuth = async (userAuth) => {
    const userDocRef = doc(db, 'users', userAuth.uid);

    const userSnapshot = await getDoc(userDocRef);

    if (!userSnapshot.exists()) {
        const { displayName, email } = userAuth;
        const createdAt = new Date();

        try {
            await setDoc(userDocRef, {
                displayName,
                email,
                createdAt,
            });
        } catch (error) {
            console.log(error);
        }
    }
    return userDocRef;
};