import { signInWithGooglePopup, createUserDocumentFromAuth } from '../../utils/firebase/firebase.utils';

export const logGoogleUser = async () => {
    const { user } = await signInWithGooglePopup();
    const userDocRef =  await createUserDocumentFromAuth(user);
    console.log(userDocRef);
};
