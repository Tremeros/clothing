import React from 'react';
import { Routes } from 'react-router-dom';
import routes from './routes/routes/routes.component';



const App = () => {

    return (
        <Routes>
            {routes}
        </Routes>
    );
};

export default App;
