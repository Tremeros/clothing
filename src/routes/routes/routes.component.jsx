import React from 'react';
import { Route } from 'react-router-dom';
import Home from '../home/home.component';
import AppLayout from '../app-layout/app-layout.component';
import SignIn from '../sign-in/sign-in.component';

export default (
    <Route path='/' element={<AppLayout />}>
        <Route index element={<Home />} />
        <Route path='sign-in' element={<SignIn />} />
    </Route>
);