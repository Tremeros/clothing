import React from 'react';
import { useEffect } from 'react';
import { getRedirectResult } from 'firebase/auth';
import { logGoogleUser } from '../../utils/auth';
import { auth, signinWithGoogleRedirect, createUserDocumentFromAuth } from '../../utils/firebase/firebase.utils';

const SignIn = () => {

    useEffect(async () => {
        const response = await getRedirectResult(auth);
        if(response) {
            const userDocRef = await createUserDocumentFromAuth(response.user);
            console.log(userDocRef);
        }
    }, []);

    return (
        <>
            <h1>Sign In</h1>
            <button onClick={logGoogleUser}>Sign in with google page</button>
            <button onClick={signinWithGoogleRedirect}>Sign in with google redirect</button>
        </>
    );
};

export default SignIn;