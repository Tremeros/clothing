import React from 'react';
import { Outlet, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { ReactComponent as Crown } from '../../assets/svg/crown.svg';
import './layout.styles.scss';

const navLinks = [
    { title: 'Home', to: '/' },
    { title: 'Sign In', to: 'sign-in' },
];

const Logo = () => {
    return (
        <Link className='logo-container' to='/'>
            <Crown className='logo' />
        </Link>
    );
};

const NavLinks = ({ links }) => {
    return (
        <div className='nav-links-container'>
            {links.map(link => {
                return <Link key={link.title} className='nav-link' to={link.to}>{link.title}</Link>;
            })}
        </div>
    );
};

const AppLayout = () => {
    return (
        <>
            <div className='layout'>
                <Logo />
                <NavLinks links={navLinks} />
            </div>
            <Outlet />
        </>
    );
};

NavLinks.propTypes = {
    links: PropTypes.array,
};

export default AppLayout;